# Anagram

lists all anagrams in a given file

## Input

File with a newline separated list of words.

### Call

The program will be invoked with exactly one argument, the file-name for them words file.

### Output

Newline separated list of anagrams found in the given file.

```
input.txt Cat
Act
Dog
God Beer

> Program input.txt Cat, act
Dog, god
```

## Authors

- **Javier Rodriguez** - [LinkedIn](https://www.linkedin.com/in/javierrodriguezb/)

## License

This project is licensed under the MIT License
