'use strict';

class AnagramFinder {
  constructor() {
    this.hashTable = new Map();
  }

  regularizeString(str) {
    return str
      .toLowerCase()
      .split('')
      .sort()
      .join('')
      .trim();
  }

  getAnagrams() {
    return [...this.hashTable.values()].filter(a => a.length > 1);
  }

  addString(str) {
    const regularizedString = this.regularizeString(str);
    const anagram = this.hashTable.get(regularizedString);
    if (!anagram) {
      this.hashTable.set(regularizedString, [str]);
    } else {
      this.hashTable.set(regularizedString, anagram.concat([str]));
    }
  }
}

module.exports = new AnagramFinder();
