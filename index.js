const path = require('path');
const lineReader = require('readline');
const fs = require('fs');
const anagramFinder = require('./lib/anagram-finder');

const SOURCE_PATH = './sources';

// Validate input argument
if (!process.argv[2]) process.exit(1);

const anagramsFile = process.argv[2];

// adding fs as linereader interface
const lr = lineReader.createInterface({
  input: fs.createReadStream(path.join(SOURCE_PATH, anagramsFile))
});

// get Anagram list from file
async function getAnagramList() {
  return new Promise((resolve, reject) => {
    lr.on('line', str => {
      anagramFinder.addString(str);
    })
      .on('close', () => {
        resolve(anagramFinder.getAnagrams());
      })
      .on('error', err => {
        reject(err);
      });
  });
}

// Main
const main = async () => {
  try {
    const anagramList = await getAnagramList();
    for (const anagram of anagramList) {
      console.log(anagram.join(', '));
    }
  } catch (error) {
    console.error(error);
  }
};

main();
